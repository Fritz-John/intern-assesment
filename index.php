<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Internship Assessment Exam</title>
</head>

<style>


    </style>
<body>



<div class="wrapper">
    <div class = "one">
         <h1 style="padding:10px;">My First Internship Assessment</h1>
    </div>
   <div class ="two">
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa aspernatur provident praesentium vero voluptate facilis quisquam nemo repellat. Nihil, aspernatur nesciunt? Debitis animi ullam vel odio! Rerum esse sed minima!
                <a href="https://www.linkedin.com/in/fritzsaycon/"> My Linkedin</a>
        </p>
        <p>
                Font is Arial </br>
                Button background is blue.
        </p>
    <!-- PHP ARRAY VALUES -->
        <?php
            $array = array("the", "quick", "brown", "fox");
            $i = 0;  
        ?>

        <!-- PHP FUNCTIONS -->
        <?php
        $num1= $num2 = $num3 = 0;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $num1 = $_POST['num1'];
            $num2 = $_POST['num2'];
            $num3 = $_POST['num3'];           
            }   

            function sum($num1,$num2, $num3){
                $array = array($num1,$num2, $num3);
                return array_sum($array);
            } 
            function average($num1,$num2, $num3){
                $array = array($num1,$num2, $num3);
                $average = array_sum($array) / count($array);
                return $average; 
            } 
            function minvalue($num1,$num2, $num3){
                $array = array($num1,$num2, $num3);
                return min($array);
            } 
            function maxvalue($num1,$num2, $num3){
                $array = array($num1,$num2, $num3);
                return max($array);
            } 
        ?>

        <!-- SHOW ARRAY VALUES AS AN UNORDERED LIST-->    
        <?php while($i < count($array)):?>
            <ul>
                <li>
                    <?php echo $array[$i]; 
                        $i++;
                    ?>          
                </li>
            </ul>   
        <?php endwhile; ?>

        <div class ="form" style="padding:10px">
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                Number 1: <input type="number" id="num1" name="num1" required><br><br>
                Number 2: <input type="number" id="num2" name="num2" required><br><br>
                Number 3: <input type="number" id="num3" name="num3" required><br><br>
                <input type="submit">
            </form>
        </div>
        <div class ="form" style="padding:10px">
                Sum: <?php echo sum($num1,$num2, $num3)?></br>
                Average: <?php echo average($num1,$num2, $num3)?></br>
                Min value: <?php echo minvalue($num1,$num2, $num3)?></br>
                Max value: <?php echo maxvalue($num1,$num2, $num3)?></br>
        </div>
    </div>
    <div class="three">
        <div style="text-align:center">
            <button class="button" onclick="alert('Hello World!')">Click me!</button>
        </div>       
    </div>
</body>
</html>
